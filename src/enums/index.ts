/**
 * 嵌套路由层级
 *
 * @enum { Father = '一级菜单',Children = '二级菜单',Grandson = '三级菜单',}
 */
export enum Grade {
  Father = '一级菜单',
  Children = '二级菜单',
  Grandson = '三级菜单',
}

/**
 * 内置颜色选项
 *
 * @export
 * @enum {number}
 */
export enum ColorType {
  default = 'default',
  primary = 'primary',
  info = 'info',
  danger = 'danger',
}
