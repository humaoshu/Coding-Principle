import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Layout from '@/layouts/BaseLayout.vue';
import { Grade } from '@/enums/index';

Vue.use(Router);

export default new Router({
  scrollBehavior: (to, from, savedPosition) => {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: { title: '首页', icon: '#iconinstruction', grade: Grade.Father },
    },
    {
      path: '/brushProblem',
      name: 'brushProblem',
      component: Layout,
      meta: {
        title: '刷题啊',
        icon: '#icon-JSX',
        grade: Grade.Father,
        module: 'sport',
      },
      children: [
        {
          path: '/jsBasics',
          component: () => import('@/views/brushProblem/JsBasics.vue'),
          meta: {
            title: '夯实基础',
            icon: '#icon-TRANSFERLEARNING',
            grade: Grade.Children,
          },
        },
        {
          path: '/objectAttribute',
          component: () =>
            import('@/views/brushProblem/jsBasics/ObjectAttribute.vue'),
          meta: {
            title: '对象属性',
            icon: '#icon-TRANSFERLEARNING',
            grade: Grade.Children,
          },
        },
        {
          path: '/variableLifting',
          component: () =>
            import('@/views/brushProblem/jsBasics/VariableLifting.vue'),
          meta: {
            title: '变量提升',
            icon: '#icon-TRANSFERLEARNING',
            grade: Grade.Children,
          },
        },

        {
          path: '/scopeClosure',
          component: () =>
            import('@/views/brushProblem/jsBasics/ScopeClosure.vue'),
          meta: {
            title: '作用域闭包',
            icon: '#icon-TRANSFERLEARNING',
            grade: Grade.Children,
          },
        },
        {
          path: '/jsAlgorithm',
          component: () => import('@/views/brushProblem/JsAlgorithm.vue'),
          meta: {
            title: '算法',
            icon: '#icon-ALGORITHM',
            grade: Grade.Children,
          },
        },
        {
          path: '/falseValue',
          component: () =>
            import('@/views/brushProblem/jsBasics/FalseValue.vue'),
          meta: {
            title: '类型转换',
            icon: '#icon-ALGORITHM',
            grade: Grade.Children,
          },
        },
        {
          path: '/regExp',
          component: () =>
            import('@/views/brushProblem/jsBasics/RegExp.vue'),
          meta: {
            title: '正则表达式',
            icon: '#icon-TRANSFERLEARNING',
            grade: Grade.Children,
          },
        },
      ],
    },
    {
      path: '/jsExplore',
      name: 'jsExplore',
      redirect: '/compilingPrinciple',
      component: Layout,
      meta: {
        title: 'js探究之路',
        icon: '#icon-JSX',
        grade: Grade.Father,
        module: 'sport',
      },
      children: [
        {
          path: '/compilingPrinciple',
          component: () => import('@/views/jsExplore/CompilingPrinciple.vue'),
          meta: {
            title: '编译原理浅析',
            icon: '#icon-TRANSFERLEARNING',
            grade: Grade.Children,
          },
        },
        {
          path: '/actionScope',
          component: () => import('@/views/jsExplore/ActionScope.vue'),
          meta: {
            title: '理解作用域',
            icon: '#icon-ALGORITHM',
            grade: Grade.Children,
          },
        },
      ],
    },
    {
      path: '/cssNote',
      name: 'cssNote',
      component: Layout,
      meta: {
        title: 'css踏坑之旅',
        icon: '#icon-CSS',
        grade: Grade.Father,
        module: 'css',
      },
      children: [
        {
          path: '/WebkitLineClamp',
          component: () => import('@/views/cssNote/WebkitLineClamp.vue'),
          meta: {
            title: '文字溢出隐藏',
            icon: '#icon-book',
            grade: Grade.Children,
          },
        },
        {
          path: '/typingAnimation',
          component: () => import('@/views/cssNote/TypingAnimation.vue'),
          meta: {
            title: '打字动画',
            icon: '#icon-book',
            grade: Grade.Children,
          },
        },
      ],
    },
    {
      path: '/movie',
      name: 'movie',
      component: Layout,
      meta: {
        title: '电影',
        icon: '#icon-iMoviedianying',
        grade: Grade.Father,
        module: 'movie',
      },
      children: [
        {
          path: '/interplanetaryCrossing',
          component: () => import('@/views/movie/InterplanetaryCrossing.vue'),
          meta: {
            title: '星际穿越',
            icon: '#icon-yuhangyuan',
            grade: Grade.Children,
          },
        },
        {
          path: '/hidamarinoKanojo',
          component: () => import('@/views/movie/HidamarinoKanojo.vue'),
          meta: {
            title: '向阳处的她',
            icon: '#icon-yangguang',
            grade: Grade.Children,
          },
        },
        {
          path: '/manchesterByTheSea',
          component: () => import('@/views/movie/ManchesterByTheSea.vue'),
          meta: {
            title: '海边的曼彻斯特',
            icon: '#icon-haitan',
            grade: Grade.Children,
          },
        },
      ],
    },
    {
      path: '/sports',
      name: 'sports',
      component: Layout,
      meta: {
        title: '体育',
        icon: '#icon-sports_mode',
        grade: Grade.Father,
        module: 'sport',
      },
      children: [
        {
          path: '/football',
          component: () => import('@/views/sports/Football.vue'),
          meta: {
            title: '足球',
            icon: '#icon-zuqiu',
            grade: Grade.Children,
          },
        },
        {
          path: '/basketball',
          component: () => import('@/views/sports/Basketball.vue'),
          meta: {
            title: '篮球',
            icon: '#icon-lanqiu',
            grade: Grade.Children,
          },
        },
      ],
    },
    {
      path: '/books',
      name: 'books',
      component: Layout,
      meta: {
        title: '书籍',
        icon: '#icon-shuji',
        grade: Grade.Father,
        module: 'books',
      },
      children: [
        {
          path: '/cssSecrets',
          component: () => import('@/views/books/CssSecrets.vue'),
          meta: {
            title: 'CSS揭秘',
            icon: '#icon-secret',
            grade: Grade.Children,
          },
        },
        {
          path: '/typescript',
          component: () => import('@/views/books/Typescript.vue'),
          meta: {
            title: '深入理解typescript',
            icon: '#icon-secret',
            grade: Grade.Children,
          },
        },
        {
          path: '/javaScriptyouDon\'tKnow',
          component: () => import('@/views/books/JavaScriptyouDon\'tKnow.vue'),
          meta: {
            title: '你不知道的JavaScript',
            icon: '#icon-JSX',
            grade: Grade.Children,
          },
        },
        {
          path: '/javaScriptAuthorityGuide',
          component: () => import('@/views/books/JavaScriptAuthorityGuide.vue'),
          meta: {
            title: 'JavaScript权威指南',
            icon: '#icon-banshizhinan',
            grade: Grade.Children,
          },
        },
        {
          path: '/profundityRxjs',
          component: () => import('@/views/books/ProfundityRxjs.vue'),
          meta: {
            title: '深入浅出Rxjs',
            icon: '#icon-RX',
            grade: Grade.Children,
          },
        },
      ],
    },
    {
      path: '/vue',
      name: 'vue',
      component: Layout,
      meta: {
        title: 'vue原理解析',
        icon: '#icon-Vue',
        grade: Grade.Father,
        module: 'code',
      },
      children: [
        {
          path: '/vuePrinciple2',
          component: () => import('@/views/vuePrinciple/VuePrinciple2.vue'),
          meta: {
            title: 'vue2.0+原理解析',
            icon: '#icon-tansuo',
            grade: Grade.Children,
          },
        },
        {
          path: '/vuePrinciple3',
          component: () => import('@/views/vuePrinciple/VuePrinciple3.vue'),
          meta: {
            title: 'vue3.0+原理解析',
            icon: '#icon-tansuo',
            grade: Grade.Children,
          },
        },
      ],
    },
    {
      path: '/rxjs',
      name: 'rxjs',
      component: Layout,
      meta: {
        title: 'rxjs实战',
        icon: '#icon-RX',
        grade: Grade.Father,
        module: 'code',
      },
      children: [
        {
          path: '/drag',
          component: () => import('@/views/rxjsExample/Drag.vue'),
          meta: {
            title: '拖拽功能',
            icon: '#icon-drag',
            grade: Grade.Children,
          },
        },
        {
          path: '/throttle',
          component: () => import('@/views/rxjsExample/Throttle.vue'),
          meta: {
            title: '按钮防抖',
            icon: '#icon-drag',
            grade: Grade.Children,
          },
        },
      ],
    },
    {
      path: '/sortingAlgorithm',
      name: 'sortingAlgorithm',
      component: Layout,
      meta: {
        title: '排序算法',
        icon: '#icon-jiyinpailie',
        grade: Grade.Father,
        module: 'code',
      },
      children: [
        {
          path: '/bubbleSort',
          component: () => import('@/views/sortingAlgorithm/BubbleSort.vue'),
          meta: {
            title: '冒泡排序',
            icon: '#icon-diguifuwuqi',
            grade: Grade.Children,
          },
        },
        {
          path: '/bucketSort',
          component: () => import('@/views/sortingAlgorithm/BucketSort.vue'),
          meta: {
            title: '桶排序',
            icon: '#icon-Bucket',
            grade: Grade.Children,
          },
        },
        {
          path: '/insertSort',
          component: () => import('@/views/sortingAlgorithm/InsertSort.vue'),
          meta: {
            title: '插入排序',
            icon: '#icon-charulie',
            grade: Grade.Children,
          },
        },
        {
          path: '/heapSort',
          component: () => import('@/views/sortingAlgorithm/HeapSort.vue'),
          meta: {
            title: '堆排序',
            icon: '#icon-anjiangxupailie',
            grade: Grade.Children,
          },
        },
        {
          path: '/mergeSort',
          component: () => import('@/views/sortingAlgorithm/MergeSort.vue'),
          meta: {
            title: '归并排序',
            icon: '#icon-anshengxupailie',
            grade: Grade.Children,
          },
        },
      ],
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('@/views/About.vue'),
      meta: { title: '个人简历', icon: '#iconinstruction', grade: Grade.Father },
    },
  ],
});
