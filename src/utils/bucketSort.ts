/**
 * 桶排序基本实现
 *
 * @param {number[]} arr
 */
function bucketSort(arr: number[]) {
  let max = Number.MAX_VALUE;
  let min = Number.MIN_VALUE;
  for (let i = 0; i < array.length; i++) {
    max = Math.max(max, arr[i]);
    min = Math.min(min, arr[i]);
  }

  const bucketNum = (max - min) / arr.length + 1;
  // 初始化桶数组
  const bucketArr: number[][] = new Array(bucketNum);
  for (let i = 0; i < bucketNum; i++) {
    bucketArr.push(new Array<number>());
  }

  // 放入所有元素
  for (const bucket of bucketArr) {
    bucket.sort();
  }
  let j = 0;
  // 合并值
  for (const tempArr of bucketArr) {
    for (const temp of tempArr) {
      arr[j++] = temp;
    }
  }
}
