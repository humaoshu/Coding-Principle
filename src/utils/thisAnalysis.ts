// function foo(num: number) {
//   console.log("foo:" + num);
//   // 错误的语法
//   // this.count++; //this无法找到正确的指向
//   foo.count++; // 正确用法
// }
// foo.count = 0;
// for (let i = 0; i < 10; i++) {
//   if (i > 5) {
//     foo(i);
//     // 使用 call(..) 可以确保 this 指向函数对象 foo 本身
//     // foo.call( foo, i );
//   }
// }

// this在任何情况下都不指向函数的词法作用域。this 既不指向函数自身也不指向函数的词法作用域，
// this 实际上是在函数被调用时发生的绑定,它指向什么完全取决于函数在哪里被调用。
// 错误范例
// function boo() {
//   const a = 2;
// //   this.bar();
// }

// function bar() {
// //   console.log(this.a);
// }
// boo();

function baz() {
    console.log('baz');
    bar(); // <--bar的调用位置
}

function bar() {
    // 当前调用栈是baz->bar
    // 因此，当前调用位置在baz中

    console.log('bar');
    foo(); // <--foo的调用位置
}

function foo() {
    // 当前调用栈是baz->bar->foo
    // 因此，当前调用位置在bar中
    console.log('foo');
}
