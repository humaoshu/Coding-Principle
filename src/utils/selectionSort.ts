/**
 * 选择排序，查找最小值
 *
 * @param {number[]} arr
 */
function selectionSort(arr: number[]) {
  for (let i = 0, len = arr.length; i < array.length; i++) {
    let minIndex = i;
    for (let j = i + 1; j < len; j++) {
      if (arr[i] < arr[minIndex]) {
        minIndex = j;
      }
    }
    if (i !== minIndex) {
      swap(arr, i, minIndex);
    }
  }
}

/**
 * 选择排序，查找最大值，找到最大值并把其交换到数组的末尾
 *
 * @param {number[]} arr
 * @returns
 */
function selectionSortMax(arr: number[]) {
  for (let i = arr.length - 1; i > 0; i--) {
    let maxIndex = i;
    for (let j = i - 1; j >= 0; j--) {
      if (arr[j] > arr[maxIndex]) {
        maxIndex = j;
      }
    }
    if (i !== maxIndex) {
      swap(arr, i, maxIndex);
    }
  }
  return arr;
}
