Function.prototype.call = function (content) {
  content = content || window; // 当content为null是，其值为window
  content.fn = this; // this

  let arg = [...arguments].slice(1);

  let result = content.fn(...arg);
  delete content.fn;
  return result;
};

function add(c, d) {
  return this.a + this.b + c + d;
}

var obj = { a: 1, b: 2 };

console.log(add.call(obj, 3, 4));


Function.prototype.apply = function (context) {
  context = context || window

  context.fn = this;

  let arg = arguments[1] || []

  let result = context.fn(...arg)

  delete context.fn

  return result;
}

Function.prototype.bind = function (context) {
  let that = this;

  let arg = [...arguments].slice(1)

  return function fn() {
    return that.apply(context, arg.concat(...arguments))
  }
}