function flip(arr: number[], start: number, leng: number) {
  let index = 0;
  for (let i = start; i < (start + arr.length) / 2; i++) {
    const temp = arr[i];
    arr[i] = arr[arr.length - index - 1];
    arr[arr.length - index - 1] = temp;
    index++;
  }
}

function pancakeSort(arr: number[]) {
  for (let i = 0, len = arr.length; i < arr.length - 1; i++) {
    const currArr = arr.slice(i, len);
    const currMax = currArr.reduce(
      (prev, curr, idx) => (curr > prev.val ? { idx, val: curr } : prev),
      {
        idx: 0,
        val: currArr[0],
      },
    );
    if (currMax.idx !== 0) {
      flip(arr, currMax.idx + i, len);
      flip(arr, i, len);
    }
  }
}
