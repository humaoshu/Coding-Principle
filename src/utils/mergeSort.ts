/**
 * 归并排序,归并排序使用分而治之的思想，以折半的方式来递归/迭代排序元素，
 * 利用空间来换时间，做到了时间复杂度 O(n·log(n)) 的同时保持了稳定。
 * 这让它在一些更考虑排序效率和稳定性，次考虑存储空间的场合非常适用
 * 如数据库内排序，和堆排序相比，归并排序的稳定是优点）。并且归并排序非常适合于链表排序。
 *
 * @param {number[]} arr
 * @returns
 */
function mergeSort(arr: number[]): any[] {
  const len = arr.length;
  if (len < 2) {
    return arr;
  }
  const mid = Math.floor(len / 2);
  //   const left = arr.slice(0, mid); // 从所传参数返回数组，参数可选
  //   const right = arr.slice(mid);

  // 空间优化
  const left = arr.splice(0, mid);
  const right = arr;

  return merge(mergeSort(left), mergeSort(right));
}

function merge(left: number[], right: number[]) {
  const result = [];
  while (left.length > 0 && right.length > 0) {
    result.push(left[0] <= right[0] ? left.shift() : right.shift());
  }
  return result.concat(left, right);
}
