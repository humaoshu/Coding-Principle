

/**
 * 快速排序
 *
 * @param {number[]} arr
 * @returns {number[]}
 */
function quickSort(arr: number[]): number[] {
  const pivot = arr[0];
  const left = [];
  const right = [];

  if (arr.length < 2) {
    return arr;
  }
  for (let i = 0, len = arr.length; i < len; i++) {
    arr[i] < pivot ? left.push(arr[i]) : right.push(arr[i]);
  }
  return quickSort(left).concat([pivot], quickSort(right));
}
