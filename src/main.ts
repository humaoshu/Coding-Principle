import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import 'ant-design-vue/dist/antd.css';
import Antd from 'ant-design-vue';
import hljs from 'highlight.js';
import 'highlight.js/styles/monokai.css';
import './styles/index.less'; // 字体等公共样式
// import effects from 'vue-effects';
// import 'vue-effects/lib/vue-effects.css';
// Vue.use(effects);
Vue.directive('highlight', el => {
  const blocks = el.querySelectorAll('pre code');
  blocks.forEach(block => {
    hljs.highlightBlock(block);
  });
});
Vue.use(Antd);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
